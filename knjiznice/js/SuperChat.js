var SuperChatAPI = {

  room: "Skedenj",

  baseURL: "https://chat.data-lab.si/api",

  lastMessageId: 0,

  getMessages: function(from, callback){

    $.get(SuperChatAPI.baseURL + "/messages/"+SuperChatAPI.room+"/"+from, function(data){
      callback(data);
    });

  },

  getNewMessages: function(callback){

    $.get(SuperChatAPI.baseURL + "/messages/"+SuperChatAPI.room+"/"+SuperChatAPI.lastMessageId, function(data){
      if(data && data[data.length-1])
        SuperChatAPI.lastMessageId = data[data.length-1].id+1;
      callback(data);
    });

  },


  getRooms: function(callback){

    $.get(SuperChatAPI.baseURL + "/rooms", function(data){
      callback(data);
    });

  },

  postMessage: function(uname, text, callback){

    $.ajax(
      {
      url: SuperChatAPI.baseURL + "/messages/" + SuperChatAPI.room,
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({
        user:  {name: uname, id: 12},
        text: text
      }), 
      success: function(data){
      callback(data);
      }
     }
    );

  },

  getUsers: function(callback){
    $.get(SuperChatAPI.baseURL + "/users", function(data){
      callback(data);
    });
  }

}
