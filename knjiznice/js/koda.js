var baseUrl = 'https://rest.ehrscape.com/rest/v1';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {

  ehrId = [
		"634ab178-2b05-4738-83c1-49744ce2cbbb", // Peter Komoda
		"0feb5e0b-643d-4c40-98fa-a2bd3a14b3c6", // Teja  Hočevar
		"2e796af2-d763-4a3c-bc39-82a932e9f894"  // Miran Tancer
	];
		
	stPacienta = parseInt(stPacienta);

	if(stPacienta < 1 || stPacienta > 3) {
		console.error("A se nismo zmenil sam od 1 do 3??");
		return null;
	}

	var params  = [
		{min: 320, max: 360},
		{min: 500, max: 720},
		{min: 400, max: 650},
	];

	stPacienta -= 1;

	var podatki = [];
	for(var i = 0; i < 10; i++){
			podatki[i] = parseInt(parseInt(Math.random()*(params[stPacienta].max - params[stPacienta].min) + params[stPacienta].min) / 10) * 10;
	}

	izbranPacient = ehrId[stPacienta];

	// Preveri, če obstaja
	
	var stUspelih = 0;

	var zapisiPodatke = function(){

			console.log(izbranPacient);

			for(var i = 0; i < podatki.length; i++){

				var cajt = moment().add((4*i - 4) * 10,"minutes");
				APICommunicator.shraniPEF(izbranPacient, podatki[i], cajt.format("YYYY-MM-DDTHH:mm:ss"), function(r){
					console.log(r);
					stUspelih++;
					if(stUspelih >= 10)
						callback(izbranPacient);	
				});

			}
	}

	APICommunicator.aliObstajaEHR(izbranPacient, function(result){

		if(!result){
		
			switch(stPacienta){

				case 0:
					APICommunicator.ustvariPacienta("Peter", "Komoda", "MALE", moment("1975-04-27T00:00:00"),74, 170, function(ehr){
						izbranPacient = ehr;	
						zapisiPodatke();
					});
				break;

				case 1:
					APICommunicator.ustvariPacienta("Miran", "Tancer", "MALE", moment("1958-01-20T22:03:20"),95, 174, function(ehr){
						izbranPacient = ehr;	
						zapisiPodatke();
					})
				break;

				case 3:
  				APICommunicator.ustvariPacienta("Teja", "Hočevar", "FEMALE", moment("1965-08-12T12:45:31"),78, 168, function(ehr){
						izbranPacient = ehr;	
						zapisiPodatke();
					})
				break;

			}

		}
		
		else {
		
			zapisiPodatke();

		}

	});
	
	
}


// API Communicator

var APICommunicator = {

	getAuthString: function(){
		return "Basic "+btoa(username+":"+password);
	},

	preveriTemplate: function(callback){

		APICommunicator.quickAjax("/template", "GET", null, null, function(success){

			var list = success.templates;
			for(var i = 0; i < list.length; i++){
				if(list[i].templateId == "Astma"){
					callback(true);
					return;
				}
			}

			// Get template and upload it
			$.get("https://lsuhadolnik.bitbucket.io/Astma.opt", function(file){

				$.ajax(baseUrl + "/template", {
					type: "POST",
					headers: {"Authorization" : APICommunicator.getAuthString(), "Content-Type":"application/xml"},
					data: file,
					success: function(a){

						console.log("Uspelo je");
						callback(true);
						return;

					},
					error: function(err){
						console.error("Napaka pri nalaganju templejta...");
						console.error(err);	
						callback(false);
						return;
					}
				})

			});

			callback(false);
			return;

		}, function(fail){
			
			console.error("Napaka pri komunikaciji...");	
			console.error(fail);
		
		});

	},

	

  getSessionId: function() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
  },

	ustvariPacienta(ime, priimek, spol, rojstniDan, teza, visina, callback){

		// first make the ehrId
	
		var partyPayload = {
			firstNames:  ime,
			lastNames:   priimek,
			gender:      spol,
			dateOfBirth: rojstniDan,
			partyAdditionalInfo: [{key: "ehrId", value: ""}]
		};

		var compositionURL = "/composition?templateId=Vital%20Signs&format=FLAT&ehrId=";
		var compositionPayload = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": moment().format("YYYY-MM-DDTHH:mm:ss"),
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		};

		var ehrid = "";

		// Ustvari nov EHR
		APICommunicator.quickAjax("/ehr", "POST", null, null, function(ehrIDdata){

			ehrId = ehrIDdata.ehrId;
			compositionURL += ehrId;
			partyPayload.partyAdditionalInfo[0].value = ehrId;		
	
			// Vnesi osebne podatke
			APICommunicator.quickAjax("/demographics/party", "POST", null, partyPayload, function(partySuccess){
				
				// Vpiši še podatke o teži in višini
				APICommunicator.quickAjax(compositionURL, "POST", null, compositionPayload, function(compositionSuccess){
		
					callback(ehrid);
					console.log("Vse je uspelo!");				
					console.log("Novi EHRId je " + ehrId);

				}, function(compositionError){

					console.error("Ni mi ratal vpisat podatkov o višini in teži");
					console.error(compositionError);
					console.log("Novi EHRID je " + ehrId);
					callback(ehrid);

				});

			}, function(partyError){
				console.error("Napaka pri vpisovanju podatkov o pacientu.");
				console.error(partyError);
				console.log("Novi EHRID je " + ehrId);
				callback(ehrid);
			});

		}, function(err){
			console.error("Napaka pri ustvarjanju EHRID.");
			console.error(err);
			callback(false);
		});


	},

	quickAjax(url, type, headers, data,  succ, erro){

				var obj = {
					url: baseUrl + url,
					type: type,
					headers: {
						"Authorization" : APICommunicator.getAuthString(),
						"Content-Type"  : "application/json"
					},
					success: succ,
					error: erro	
				}

				if(headers){
					for(var h in headers){
						obj.headers[h] = headers[h];
					}
				}

				if(data) {
					obj.data = JSON.stringify(data);
				}

				$.ajax(obj);

	},

	aliObstajaEHR(ehrid, callback){

		APICommunicator.quickAjax("/ehr/"+ehrid, "GET", null, null, function(success){

			if(success.action == "RETRIEVE")
				callback(true);

		}, function(error){

				callback(false);

		});
	
	},
  
  dobiOsebnePodatke: function(ehrid, callback){
   //:Object

		var dat = {

			datumRojstva: "",
			ime: "",
			starost: "",
			teza: "",
			visina: ""

		}

		$.ajax({

			url: baseUrl + "/demographics/ehr/"+ehrid+"/party",
			type: 'GET',
			headers: {
				"Authorization": APICommunicator.getAuthString()
			},
			success: function(p){

				dat.ime = p.party.firstNames+" "+p.party.lastNames;
				var m = new Date(p.party.dateOfBirth);
				var today = new Date();
				var starost = parseInt((today-m) / (1000 * 60 * 60 * 24 * 365));
				
				dat.datumRojstva = m;
				dat.starost = starost;

				var height = $.ajax(
					baseUrl + "/view/"+ehrid+"/height?limit=1",
					{type: 'GET',
			      headers: {"Authorization": APICommunicator.getAuthString()},
						async:false
					});

				var weight = $.ajax(
					baseUrl + "/view/"+ehrid+"/weight?limit=1",
					{type: 'GET',
			      headers: {"Authorization": APICommunicator.getAuthString()},
						async:false
					});

				height = height.responseJSON;
				weight = weight.responseJSON;

				console.log(height);

				if(height && height[0])
					dat.visina = height[0].height + " cm";

				if(weight && weight[0])
					dat.teza = weight[0].weight + " kg";

				callback(dat);

			},

			error: function(p){

				console.error(p);

			}
			
		});	
			
			

  },

  dobiDnevnePodrobnosti: function(ehrId, datum, callback){
  
  //Object

   // Dobi PEF

			
     var PEF_AQL =
						"select " +
    						"t/data[at0001]/events[at0002]/time/value as cas, " +
    						"t/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/magnitude as pef " +
						"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
						"contains OBSERVATION t[openEHR-EHR-OBSERVATION.pef.v0] " +
						//"where t/data[at0001]/events[at0002]/time/value = '"+ datum +"' " +
						"order by t/data[at0001]/events[at0002]/time desc ";

     var Zdravila_AQL =
						"select " +
    						"t/data[at0001]/events[at0002]/time/value as cas, " +
    						"t/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/value as ime, " +
    						"t/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value/magnitude as doza, " +
    						"t/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value/units as enota " +
						"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
						"contains OBSERVATION t[openEHR-EHR-OBSERVATION.zdravila.v0] " ;
						//"where t/data[at0002]/events[at0003]/time = '"+ datum +"' " +
						"order by t/data[at0001]/events[at0002]/time desc ";

			// En droben hek

			var data = {zdravila: [], pef: []};

			setTimeout(function() { // Nov Thread

					var res = $.ajax({
							async: false,
					    url: baseUrl + "/query?" + $.param({"aql": PEF_AQL}),
					    type: 'GET',
					    headers: {
								"Authorization" : APICommunicator.getAuthString()
							},
							error: function(res){
									console.error("Napaka.");
							}
					});	

					res = res.responseJSON;

					if(res && res.resultSet){

						res.resultSet.forEach(function(a) {

							if(moment(a.cas).format("YYYY-MM-DD") == moment(datum).format("YYYY-MM-DD"))
								data.pef.push(a);

						});

					}

					var res = $.ajax({
							async: false,
					    url: baseUrl + "/query?" + $.param({"aql": Zdravila_AQL}),
					    type: 'GET',
					    headers: {
								"Authorization" : APICommunicator.getAuthString()
							},
							error: function(res){
									console.error("Napaka pri pridobivanju podatkov o zdravilih.");
							}	

					});

					res = res.responseJSON;

					if(res && res.resultSet){

						res.resultSet.forEach(function(a) {

							if(moment(a.cas).format("YYYY-MM-DD") == moment(datum).format("YYYY-MM-DD"))
								data.zdravila.push(a);

						});

					}

					callback(data);
		
			}, 1); // Konec Threada



  },



  shraniPodatkeOPrejetiDozi: function(ehrid, zdravilo, doza, enota, datum, callback){

    // URL: 
			var url = "https://rest.ehrscape.com/rest/v1/composition?templateId=Astma&format=FLAT&ehrId="+ehrid;

    // BODY:

      var payload = {
      "ctx/language": "sl",
      "ctx/territory": "SI",
      "ctx/time": moment(datum).format("YYYY-MM-DDTHH:mm"),
      "astma/zdravila/ime": zdravilo,
      "astma/zdravila/doza|magnitude": doza,
      "astma/zdravila/doza|unit":enota
      }

    // HEADERS:
      
      var headers = {
        "Authorization" : APICommunicator.getAuthString(),
        "Content-Type"  : "application/json"
      }

			$.ajax({

				url: url,
				type: "POST",
				data: JSON.stringify(payload),
				headers: headers,
				success: function() {callback(true)},
				error: function(a) {callback(false); console.log("AAA!!"); console.log(a);}

			});

  },

  shraniPEF: function(ehrid, pef, datum, callback){

    // URL: 
		var url = "https://rest.ehrscape.com/rest/v1/composition?templateId=Astma&format=FLAT&ehrId=" + ehrid;

    // BODY:

      var payload = {
        "ctx/language": "sl",
        "ctx/territory": "SI",
        "ctx/time": moment(datum).format("YYYY-MM-DDTHH:mm"),
        "astma/pef/pef|magnitude":pef,
        "astma/pef/pef|unit":"l/min",
        "astma/pef/cas":datum
      }
    // HEADERS:
      
      var headers = {
        "Authorization" : APICommunicator.getAuthString(),
        "Content-Type"  : "application/json"
      }

		$.ajax({

			url: url,
			type: "POST",
			data: JSON.stringify(payload),
			headers: headers,
			success: function()  {callback(true);},
			error:   function(a) {callback(false); console.log("AAA!"); console.log(a);}

		});

  }

};
